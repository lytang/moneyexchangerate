import {combineReducers} from 'redux'
import exchangeRate from './exchangeRate'

const rootReducers = combineReducers({
	exchangeRate
})

export default rootReducers