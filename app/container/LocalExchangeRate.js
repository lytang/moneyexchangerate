import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import moment from 'moment'
const data = [
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Australian Dollar',
    currency:'AUD',
    imageUrl: 'https://www.countryflags.io/AU/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Chinese Yuan',
    currency:'CNY',
    imageUrl: 'https://www.countryflags.io/CN/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Euro',
    currency:'EUR',
    imageUrl: 'https://www.countryflags.io/EU/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'British Pound',
    currency:'GBP',
    imageUrl: 'https://www.countryflags.io/GB/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Hong Kong Dollar',
    currency:'HKD',
    imageUrl: 'https://www.countryflags.io/HK/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Indonesia Rupiah',
    currency:'IDR',
    imageUrl: 'https://www.countryflags.io/ID/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Japanese Yen',
    currency:'JPY',
    imageUrl: 'https://www.countryflags.io/JP/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Khmer Riel',
    currency:'KHR',
    imageUrl: 'https://www.countryflags.io/KH/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'South Korean Won',
    currency:'KRW',
    imageUrl: 'https://www.countryflags.io/KR/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'New Zealand Dollar',
    currency:'NZ$',
    imageUrl: 'https://www.countryflags.io/NZ/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Philippines Peso',
    currency:'PHP',
    imageUrl: 'https://www.countryflags.io/PH/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Singapore Dollar',
    currency:'SGD',
    imageUrl: 'https://www.countryflags.io/SG/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Thailand Baht',
    currency:'THB',
    imageUrl: 'https://www.countryflags.io/TH/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'United State Dollar',
    currency:'',
    imageUrl: 'https://www.countryflags.io/US/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Vietnam Dong',
    currency:'VND',
    imageUrl: 'https://www.countryflags.io/VN/flat/64.png'//:country_code/:style/:size.png
  },
  {
    rateIn: 4085,
    rateOut:4090,
    name:'Taiwan Dollar',
    currency:'TWD',
    imageUrl: 'https://www.countryflags.io/TW/flat/64.png'//:country_code/:style/:size.png
  },
]
class LocalExchangeRate extends Component{
	static navigationOptions = {
    title: 'Local Exchange Rate',
    headerTintColor: '#fff',
    headerStyle: {
        backgroundColor: '#026791',
      },
    headerTitleStyle: {
      fontWeight: 'bold',
      // color:'#fff'
    },
    /* No more header config here! */
  };

  /* render function, etc */
	render(){
		return (
			<View style={{flex:1}}>
				<Text style={{width:'100%', textAlign: 'center'}}>Local Exchange Rate</Text>
        <View style={{flexDirection: 'row', marginLeft:20,paddingTop:10, flex:0.1}}>
          <Text style={{flex:6}}>{moment().format('YYYY-MM-DD HH:mm:ss')}</Text>
          <Text style={{flex:2,fontSize:18, paddingLeft:5, paddingRight:5, color:'blue'}}>Buy</Text>
          <Text style={{flex:2,fontSize:18, paddingLeft:5, paddingRight:5, color:'blue', marginLeft:20}}>Sell</Text>
        </View>
        <ScrollView>
          {data.map((country,index)=>
            <View key = {index} style={{flexDirection: 'row', marginLeft:20,paddingTop:10}}>
              <Image source= {{uri:country.imageUrl}} style={{width:50, height:50}}/>
              <View style={{marginTop:5, marginLeft:20, flex:1}}>
                <Text style={{fontSize:18, color:'blue'}}>{country.currency}</Text>
                <Text>{country.name}</Text>
              </View>
              <View style={{flexDirection: 'row', marginRight:40,paddingTop:10, flex:1, justifyContent: 'space-between'}}>
                <Text style={{fontSize:18, color:'blue'}}> {country.rateIn} </Text>
                <Text style={{fontSize:18, color:'blue'}}> {country.rateOut}</Text>
              </View>
            </View>
            )
          }
        </ScrollView>
			</View>
		)
	}
}

export default LocalExchangeRate;