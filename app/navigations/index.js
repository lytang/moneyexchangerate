import {createStackNavigator, createDrawerNavigator} from 'react-navigation';
import LocalExchangeRate from '../container/LocalExchangeRate';
import InternationalExchangeRate from '../container/InternationalExchangeRate';

export default AppNavigator = createStackNavigator({
	LocalExchangeRate:{
		screen:LocalExchangeRate
	},
	InternationalExRate:{
		screen: InternationalExchangeRate
	}
});

// export AppNavigator;

