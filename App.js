import React, { Component } from 'react';
import {Platform,StyleSheet,Text,View} from 'react-native';
// import MixedNavigator from './src/navigation/MixedNavigator'
import AppNavigator from './app/navigations'
import configureStores from './app/config/configuration'
import {Provider} from 'react-redux'
// import SplashScreen from 'react-native-splash-screen'

let store = configureStores()

// const App =()=>(
//      <MixedNavigator />
//  )
class App extends Component {
  componentDidMount() {
    // // console.tron.log('work')
    // if(Platform.OS == 'android'){
    //   SplashScreen.hide();
    // }
  }
  render() {
    return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
    );
  }
}
export default App